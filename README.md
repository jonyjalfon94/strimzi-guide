# Strimzi-kafka guide

### Install the operator using the official Helm chart

Add the strimzi repository to Helm repositories

```
helm repo add strimzi https://strimzi.io/charts/
```

Install the strimzi operator
* Note that we are creating a namespace called kafka-system where the operator resources will be deployed

```
helm install strimzi-operator strimzi/strimzi-kafka-operator --values=./helm/strimzi-operator-values.yaml --create-namespace -n kafka-system
```
### Deploying a cluster

Edit the kafka.yaml according to your needs and apply into a namespace wheree the operator is watching
```
kubectl apply -f ./resources/kafka.yaml -n <kafka-ns> 
```

You can inspect kafka configs by inspecting the ConfigMap generated by the operator
```
export CLUSTER_NAME=my-kafka-cluster
kubectl get -n demo configmap/${CLUSTER_NAME}-kafka-config -o yaml
```

### Deploying Users and Topics

After deploying the kafka cluster we can start deploying KafkaTopics and KafkaUsers using their CRDs. Check the resources folder for examples.
```
kubectl apply -f ./resources/kafka-user.yaml -n <kafka-ns>
kubectl apply -f ./resources/kafka-topic.yaml -n <kafka-ns>
```

When a user is deployed the operator will generate a secret with the name of the user that contains its keys/certs. Inspect the secret
```
kubectl describe secret <kafka-user> -n <kafka-cluster>
```

You can use the utility "create-kafka-keys.sh" to extract the files created by the operator for a specific user. 
* Use the flag --help to get more info on how to use the utility
```
./scripts/create-kafka-keys.sh --kafka-ns <kafka-ns> --kafka-cluster <kafka-cluster> --kafka-user <kafka-user> --keystore-pass <password>
```

### Debug

Check operator's logs
```
kubectl logs -n <operator-ns> <operator-pod>
```

To debug you can use the "zoo-entrance" in debug-tools to connect "kafka-tool" to the cluster. Remember to edit the file and add YOUR names.
* Note that this will make zookeeper accesible from the outside thus insecure
* Not reccomended for production environments
* If used in a production environment remember to cleanup (delete all the tool's resources) to make zookeeper secure again
```
kubectl apply -f ./debug-tools/zoo-entrance.yaml -n <kafka-ns>
```
