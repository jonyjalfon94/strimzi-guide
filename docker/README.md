# Adding plugins for kafka connect

To add plugins to the kafka connect instance follow this instructions

* Create a custom docker image
* Add the desired plugins and push the image to a repository.
* In the values file of the operator change the following section:
```
kafkaConnect:
  image:
    repository: <your-repository>
    name: <your-image>
    tagPrefix: <your-tag>
```
* After changing the values file update the Helm release with the new values
```
helm upgrade <release-name> strimzi/strimzi-kafka-operator --values <path/to/values> -n kafka-system
```