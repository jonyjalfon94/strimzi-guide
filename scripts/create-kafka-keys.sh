#!/bin/bash

#DEBUG
set -x

#=====================================================
# PARAMETERS
#=====================================================

# user input
KAFKA_NS=""
KAFKA_USER=""
KEYSTORE_PASSWORD=""
TRUSTSTORE_PASSWORD=""
KAFKA_CLUSTER_NAME=""

# internal
USER_KEY_PASSWORD_FILE=""
USER_P12_FILE=""
KEYSTORE_FILE=""
TRUSTSTORE_FILE=""  
CA_CERT_FILE="" 
CA_CERT_PASSWORD=""
USER_CERT_FILE=""
USER_KEY_FILE=""



#=====================================================
# FUNCTIONS
#=====================================================

# help function

function help
{
    echo ''
    echo '=================================='
    echo '  Credentials-Extractor  '
    echo '=================================='
    echo ''
    echo 'DESCRIPTION'
    echo '-----------'
    echo ''
    echo 'This script is used to extract the auto-generated credentials after'
    echo 'deploying a KafkaUser with tls authentication'
    echo ''
    echo 'PREREQUISITES'
    echo '----------'
    echo '- java sdk (for keytools) '
    echo ''
    echo 'PARAMETERS'
    echo '----------'
    echo '--kafka-ns: Namespace where the Kafka cluster is deployed'
    echo '--kafka-user: The name of the Kafka user'
    echo '--kafka-cluster: Name of the Kafka cluster'
    echo '--keystore-pass: Password used to lock the Keystore and Truststore'
    echo ''
    echo 'EXAMPLE'
    echo '----------'
    echo './create-kafka-keys.sh --kafka-ns <kafka-ns> --kafka-cluster <kafka-cluster> --kafka-user <kafka-user> --keystore-pass <password>'
    echo '----------'
    echo ''
    
    exit
}

# parameters management functions

function load-parameters
{
    # assign parameters
    
    while [ "$1" != "" ]; do
        case "$1" in
            --kafka-ns )            KAFKA_NS="$2";           shift;;
            --kafka-user )          KAFKA_USER="$2";         shift;;
            --keystore-pass )       KEYSTORE_PASSWORD="$2";  shift;;
            --kafka-cluster )       KAFKA_CLUSTER_NAME="$2"; shift;;
            --help )                help;                     exit;; # quit and show usage
            * )                     args+=("$1")              # if no match, add it to the positional args
        esac
        shift # move to next kv pair
    done

    #internal variables
    USER_KEY_PASSWORD_FILE=${KAFKA_USER}-key.password
    USER_P12_FILE=${KAFKA_USER}.p12
    KEYSTORE_FILE=${KAFKA_USER}_keystore.jks
    TRUSTSTORE_FILE=${KAFKA_USER}_truststore  
    CA_CERT_FILE=ca.crt 
    CA_CERT_PASSWORD=ca.password
    USER_CERT_FILE=${KAFKA_USER}.crt
    USER_KEY_FILE=${KAFKA_USER}.key
}


function extract-secrets
{
    kubectl get secret $KAFKA_USER -n $KAFKA_NS -o jsonpath='{.data.user\.crt}' | base64 --decode > $USER_CERT_FILE
    kubectl get secret $KAFKA_USER -n $KAFKA_NS -o jsonpath='{.data.user\.key}' | base64 --decode > $USER_KEY_FILE
    kubectl get secret $KAFKA_USER -n $KAFKA_NS -o jsonpath='{.data.user\.p12}' | base64 --decode > $USER_P12_FILE
    kubectl get secret $KAFKA_USER -n $KAFKA_NS -o jsonpath='{.data.user\.password}' | base64 --decode > $USER_KEY_PASSWORD_FILE
}


function import-keystore
{
    PASSWORD=`cat $USER_KEY_PASSWORD_FILE`
   
    sudo keytool -importkeystore -deststorepass $KEYSTORE_PASSWORD -destkeystore $KEYSTORE_FILE -srckeystore $USER_P12_FILE -srcstorepass $PASSWORD -srcstoretype PKCS12
    sudo keytool -list -alias $KAFKA_USER -keystore $KEYSTORE_FILE -deststorepass $KEYSTORE_PASSWORD
}
function import-truststore
{
    PASSWORD=`cat $USER_KEY_PASSWORD_FILE`
    kubectl get -n $KAFKA_NS secret $KAFKA_CLUSTER_NAME-cluster-ca-cert -o jsonpath='{.data.ca\.crt}' | base64 --decode > $CA_CERT_FILE
    kubectl get -n $KAFKA_NS secret $KAFKA_CLUSTER_NAME-cluster-ca-cert -o jsonpath='{.data.ca\.password}' | base64 --decode > $CA_CERT_PASSWORD
    sudo keytool -importcert -alias strimzi-kafka-cert -deststorepass $KEYSTORE_PASSWORD -file $CA_CERT_FILE -keystore $TRUSTSTORE_FILE -keypass $PASSWORD -noprompt
    sudo keytool -list -alias strimzi-kafka-cert -keystore $TRUSTSTORE_FILE -storepass $KEYSTORE_PASSWORD
} 

function main
{
  load-parameters $@
  mkdir ${KAFKA_USER}-certs
  cd ${KAFKA_USER}-certs
  extract-secrets
  import-keystore
  import-truststore
}
#=====================================================
# SCRIPT
#=====================================================

# show help if parameters are not passed, else start script
if [ -z "$1" ]
then
    help
else
    main $@
fi